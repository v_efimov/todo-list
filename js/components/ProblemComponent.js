'use strict';

import AlertModalComponent from './AlertModalComponent.js';
import Popup from './Popup.js';

import { getData, postData } from '../handlers/api-requests.js';
import { getCookie } from '../handlers/cookies.js';
import { textOverflowInNode } from '../handlers/text-handlers.js';

class ProblemComponent { //TODO: Change the name here or in all others?
    constructor() {
        this.problem;
        this.userId;
        this.changeProblemStatusBoundFunction = this.changeProblemStatus.bind(this)
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.loginButton ? true : false;
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     * @param {*} data Problem information
     * @param {*} userId User ID for which the component is being initialized
     */
    init(location, data, userId) {
        this.userId = userId;
        const template = document.querySelector('#item');
        const fragment = document.createDocumentFragment();
        const item = template.content.cloneNode(true);
        this.problem = item.querySelector('.item');
        this.setProblemStatus(data.status);
        item.querySelector('.item__title').textContent = data.name;
        item.querySelector('.item__description').textContent = data.description;
        textOverflowInNode(item.querySelector('.item__description'), 50);
        item.querySelector('.item__date').textContent = data.date;
        item.querySelector('.item').id = data.id_problem;
        item.querySelector('.item__button').addEventListener('click', this.showProblem);
        this.problem.querySelectorAll('.item__action').forEach(element => {
            element.addEventListener('click', this.changeProblemStatusBoundFunction);
        });
        fragment.appendChild(item);
        document.querySelector(location).appendChild(fragment);
    }

    /**
     * Changes the status of a problem.
     * @param {*} e event
     */
    changeProblemStatus(e) {
        const action = e.target.id;
        this.problem = e.target.closest('.item');
        const elemStatus = this.problem.querySelector('.info-plate');
        
        if (elemStatus.classList.contains('info-plate_deleted') && (action == 'deleted')) {
            if (confirm('Are you sure you want to delete this problem permanently?')) {
                postData('./api/problem/deleteProblem.php', `user-id=${this.userId}&id=${this.problem.id}&csrf-token=${getCookie('csrf-token')}`).then(
                    (response) => {
                        this.destroyComponent();
                        document.dispatchEvent(new Event('permanent-remove'));
                    },
                    (error) => {
                        const alertModal = new AlertModalComponent();
                        alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
                    }
                )
            }                
        } else {   
            postData('./api/problem/changeProblemStatus.php', `user-id=${this.userId}&id=${this.problem.id}&status=${action}&csrf-token=${getCookie('csrf-token')}`).then(
                (response) => {
                    const listState = getCookie('list-state');
                    if( listState !== 'all') { //TODO: ? It should be here or in ToDo.js ?
                        this.destroyComponent(); 
                    } else {
                        this.setProblemStatus(action);
                    }                     
                    document.dispatchEvent(new Event('changed-status'));
                },
                (error) => {
                    const alertModal = new AlertModalComponent();
                    alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
                }
            )  
        }
    } 

    /**
     * Sets the problem to a new status.
     * @param {*} newStatus New status
     */
    setProblemStatus(newStatus) {
        const plate = this.problem.querySelector('.info-plate');
        plate.classList.remove('info-plate_active');
        plate.classList.remove('info-plate_completed');
        plate.classList.remove('info-plate_deleted');
        plate.classList.add(`info-plate_${newStatus}`);
        this.problem.classList.remove('item_completed');
        this.problem.classList.remove('item_deleted');
        this.problem.classList.remove('item_completed');
        this.problem.classList.add(`item_${newStatus}`);
    }

    /**
     * Opens a window with detailed information about the problem.
     * @param {*} e more button click event
     */
    showProblem(e) {
        getData('./api/problem/getProblemInfo.php', `id=${e.target.closest('.item').id}`).then(
            (response) => {
                const problemFullInfo = JSON.parse(response).data;
                const popup = new Popup();
                popup.open(popup.createInfoElement(problemFullInfo));
            },
            (error) => {
                const alertModal = new AlertModalComponent();
                alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
            }
        ) 
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.problem.removeEventListener('click', this.showProblem);
        this.problem.querySelectorAll('.item__action').forEach(element => {
            element.removeEventListener('click', this.changeProblemStatusBoundFunction);
        });
        this.problem.remove();
    }
}

export default ProblemComponent;