'use strict';

import CreateProblem from './CreateProblem.js';
import Login from './Login.js';
import Profile from './Profile.js';
import Registration from './Registration.js';

class Header {
    constructor() {
        this.registration = new Registration();
        this.login = new Login();
        this.profile = new Profile();
        this.createProblem = new CreateProblem();
    }

    /**
     * Creates a header for an unauthorized user.
     */
    createHeaderForUnauthorizedUser() {
        this.createProblem.isExist() && this.createProblem.destroyComponent();
        this.profile.isExist() && this.profile.destroyComponent();
        this.registration.init('.header__profile');
        this.login.init('.header__profile');
    }

    /**
     * Creates a header for an authorized user
     * @param {*} user user
     */
    createHeaderForAuthorizedUser(user) {
        this.registration.isExist() && this.registration.destroyComponent();
        this.login.isExist() && this.login.destroyComponent();
        this.createProblem.init('.header__create-problem-button', user.id_user);
        this.profile.init('.header__profile', user.firstName, user.lastName);
    } 
}

export default Header;