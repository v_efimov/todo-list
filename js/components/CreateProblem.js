'use strict';

import Popup from './Popup.js';

import AlertModalComponent from './AlertModalComponent.js';
import InputError from './InputError.js';
import Problem from '../lib/Problem.js';
import Validation from './Validation.js';

import { postData } from '../handlers/api-requests.js';
import { getCookie } from '../handlers/cookies.js';
import { resetAllErrorsInForm } from '../handlers/others.js';

class CreateProblem { //TODO: Name?
    constructor() {
        this.userId
        this.popup = new Popup();
        this.validate = new Validation();
        this.createProblemButton;
        this.createProblemForm;
        this.openCreateProblemFormBoundFunction = this.openCreateProblemForm.bind(this);
        this.createProblemSubmitHandler = this.createProblemFormHandler.bind(this);
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.createProblemButton ? true : false; 
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     * @param {*} userId User ID for which the component is being initialized
     */
    init(location, userId) {
        this.userId = userId;
        this.createProblemButton = document.createElement('button');
        this.createProblemButton.className = 'button button_primary';
        this.createProblemButton.id = 'create-problem';
        this.createProblemButton.textContent = 'Create problem';
        this.createProblemButton.addEventListener('click', this.openCreateProblemFormBoundFunction);
        document.querySelector(location).append(this.createProblemButton);
    }

    /**
     * Opens the problem creation form.
     */
    openCreateProblemForm() {
        this.popup.open(this.popup.createAddProblemForm());
        this.validate.validationCreateProblemForm();
        this.createProblemForm = document.querySelector('#create-problem-form');
        this.createProblemForm.addEventListener('submit', this.createProblemSubmitHandler);
    }

    /**
     * Handles submitting the problem creation form.
     * @param {*} e event
     */
    createProblemFormHandler(e) {
        e.preventDefault();
        const problem = new Problem( 
            document.querySelector('#name').value, 
            document.querySelector('#date').value,
            document.querySelector('#description').value,
            document.querySelector('#priority').value
        );
        if (this.validate.isProblemFormCorrect(this.createProblemForm)) {
            postData('./api/problem/addProblem.php', 
            `user-id=${this.userId}&name=${problem.name}&date=${problem.date}&priority=${problem.priority}
            &description=${problem.description}&status=${problem.status}&csrf-token=${getCookie('csrf-token')}`).then(
            (response) => {
                const responseFromBack = JSON.parse(response);
                if(responseFromBack.type === 'VALIDATION-ERROR') {
                    const errors = responseFromBack.data;
                    resetAllErrorsInForm(this.createProblemForm);
                    errors.forEach(el => {
                        const error = new InputError(el);
                        error.show(this.createProblemForm);
                    });
                } 
                if(responseFromBack.type === 'response') {
                    document.dispatchEvent(new Event('problem-created'));
                    this.createProblemForm.removeEventListener('submit', this.createProblemSubmitHandler);
                    this.validate.removeListenersValidationCreateProblemForm();
                    this.popup.close();
                    const alertModal = new AlertModalComponent();
                    alertModal.open('Success!', 'The problem has been created!');
                }
            },
            (error) => {
                const alertModal = new AlertModalComponent();
                alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
            });
        }
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.createProblemButton.removeEventListener('click', this.openCreateProblemFormBoundFunction);
        this.createProblemButton.remove();
    }
}

export default CreateProblem;