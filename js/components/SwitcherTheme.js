'use strict';

import { getCookie, setCookie } from '../handlers/cookies.js';

class SwitcherTheme {
    constructor() {
        this.switcher;
        this.theme = getCookie('theme') ? getCookie('theme') : 'dark';
        this.theme === 'dark' ? this.setDarkTheme() : this.setLightTheme();
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     */
    init(location) {
        this.switcher = document.createElement('label');
        this.switcher.className ='switch';
        this.switcher.innerHTML = `
            <input class="switch-input" type="checkbox" id="slider">
            <span class="slider round"></span>
        `;
        document.querySelector(location).append(this.switcher);
        document.querySelector('.switch-input').addEventListener('change', this.toggleTheme.bind(this));
        this.theme === 'dark' ? document.querySelector('.switch-input').checked = false : document.querySelector('.switch-input').checked = true;
    }

    /**
     * Switches the theme of the site.
     */
    toggleTheme() {
        this.theme === 'dark' ? this.setLightTheme() : this.setDarkTheme();
        setCookie('theme', this.theme, 1);
    }

    /**
     * Sets the light theme.
     */
    setLightTheme() {
        document.querySelector('body').classList.add('light');
        this.theme = 'light';
    }

    /**
     * Sets the dark theme.
     */
    setDarkTheme() {
        document.querySelector('body').classList.remove('light');
        this.theme = 'dark';
    }
}

export default SwitcherTheme;