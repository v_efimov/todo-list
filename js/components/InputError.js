'use strict';

class InputError {
    constructor(error) {
        this.message = error.message;
        this.location = error.location;
    }

    /**
     * Shows an error in the form.
     * @param {*} form form
     */
    show(form) {
        const formLabel = form.querySelector(`[name="${this.location}"]`).closest('.form__label')
        const errorNode = formLabel.querySelector('.form__error');
        errorNode.classList.add('form__error_shake');
        errorNode.textContent = this.message;
    }
}

export default InputError;