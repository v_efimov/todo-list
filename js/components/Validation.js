'use strict';

class Validation {
    constructor() {
        this.haveErrors = {
            problemName: false,
            problemPriority: false,
            problemDate: false,
            problemDescription: false,
            firstName: false,
            lastName: false,
            username: false,
            eMail: false,
            password: false,
            rePassword: false
        }

        this.validateProblemNameBoundFunction = this.validateProblemName.bind(this);
        this.validateProblemPriorityBoundFunction = this.validateProblemPriority.bind(this);
        this.validateProblemDateBoundFunction = this.validateProblemDate.bind(this);
        this.validateProblemDescriptionBoundFunction = this.validateProblemDescription.bind(this);
        this.validateFirstNameBoundFunction = this.validateFirstName.bind(this);
        this.validateLastNameBoundFunction = this.validateLastName.bind(this);
        this.validateUsernameBoundFunction = this.validateUsername.bind(this);
        this.validateEMailBoundFunction = this.validateEMail.bind(this);
        this.validatePasswordBoundFunction = this.validatePassword.bind(this);
        this.validateRePasswordBoundFunction = this.validateRePassword.bind(this);
    };
    

    /**
     * Validates the problem creation form.
     */
    validationCreateProblemForm() {
        const name = document.querySelector('#name');
        const priority = document.querySelector('#priority');
        const date = document.querySelector('#date');
        const description = document.querySelector('#description');
        name.addEventListener('input', this.validateProblemNameBoundFunction);
        priority.addEventListener('input', this.validateProblemPriorityBoundFunction);
        date.addEventListener('input', this.validateProblemDateBoundFunction);
        description.addEventListener('input', this.validateProblemDescriptionBoundFunction);
    }

    /**
     * Validates the input of a "Problem name" in a field.
     * @param {*} e field input event
     */
    validateProblemName(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if (value.match(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g)) {
            error.textContent = 'Invalid char in Name';
            this.haveErrors.problemName = true;
            return;
        }
        if(value.length < 3) {
            error.textContent = 'Name too short';
            this.haveErrors.problemName = true;
            return;
        }
        if(value.length > 25) {
            error.textContent = 'Name too long (max 25)';
            this.haveErrors.problemName = true;
            return;
        }
        this.haveErrors.problemName = false;
    }

    /**
     * Validates the input of a "Priority problem" in a field.
     * @param {*} e field input event
     */
    validateProblemPriority(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if(value !== 'Low' && value !== 'Normal' && value !== 'High') {
            error.textContent = 'Choose one of the options: Low, Normal, High';
            this.haveErrors.priority = true;
            return;
        }
        this.haveErrors.priority = false;
    }

    /**
     * Validates the input of a "Date problem" in a field.
     * @param {*} e field input event
     */
    validateProblemDate(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        var arrD = value.split("-");
        arrD[1] -= 1;
        var d = new Date(arrD[0], arrD[1], arrD[2]);
        if ((d.getFullYear() == arrD[0]) && (d.getMonth() == arrD[1]) && (d.getDate() == arrD[2])) {
        } else {
            error.textContent = 'Invalid format. Select a date from the list or enter in the format YYYY-MM-DD';
            this.haveErrors.problemDate = true;
            return;
        }
        this.haveErrors.problemDate = false;
    }

    /**
     * Validates the input of a "Description problem" in a field.
     * @param {*} e field input event
     */
    validateProblemDescription(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if(value.length > 600) {
            error.textContent = 'Description too long (max 600)';
            this.haveErrors.problemDescription = true;
            return;
        }
        this.haveErrors.problemDescription = false;
    }

    /**
     * Checks if the problem creation form is correct
     * @param {*} form form
     */
    isProblemFormCorrect(form) {
        const name = form.querySelector('#name').value;
        const priority = form.querySelector('#priority').value;
        const date = form.querySelector('#date').value;
        if (!name || !priority || !date) {
            form.querySelector('#last-error-form').textContent = 'Please complete all required form fields';
            return false;
        }
        return !this.haveErrors.problemDate && !this.haveErrors.problemName && !this.haveErrors.problemPriority && !this.haveErrors.problemDescription;
    }

    /**
     * Removes all validation handlers from the form.
     */
    removeListenersValidationCreateProblemForm() {
        const name = document.querySelector('#name');
        const priority = document.querySelector('#priority');
        const date = document.querySelector('#date');
        const description = document.querySelector('#description');
        name.removeEventListener('input', this.validateProblemNameBoundFunction);
        priority.removeEventListener('input', this.validateProblemPriorityBoundFunction);
        date.removeEventListener('input', this.validateProblemDateBoundFunction);
        description.removeEventListener('input', this.validateProblemDescriptionBoundFunction);
    }

    /**
     * Validates the registration form.
     */
    validateRegistrationForm() {
        const firstName = document.querySelector('#first-name');
        const lastName = document.querySelector('#last-name');
        const username = document.querySelector('#username');
        const eMail = document.querySelector('#e-mail');
        const password = document.querySelector('#password');
        const rePassword = document.querySelector('#repeat-password');

        firstName.addEventListener('input', this.validateFirstNameBoundFunction);
        lastName.addEventListener('input', this.validateLastNameBoundFunction);
        username.addEventListener('input', this.validateUsernameBoundFunction);
        eMail.addEventListener('input', this.validateEMailBoundFunction);
        password.addEventListener('input', this.validatePasswordBoundFunction);
        rePassword.addEventListener('input', this.validateRePasswordBoundFunction);
    }

    /**
     * Validates the input of a "First name" in a field.
     * @param {*} e field input event
     */
    validateFirstName(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if(value === '') {
            error.textContent = '';
            return;
        }
        if (!value.match(/^[a-zA-Z,.-]+$/)) {
            error.textContent = 'Invalid char in First name';
            this.haveErrors.firstName = true;
            return;
        }
        if(value.length > 25) {
            error.textContent = 'First name too long';
            this.haveErrors.firstName = true;
            return;
        }
        if(value.length < 3) {
            error.textContent = 'First name too short';
            this.haveErrors.firstName = true;
            return;
        }
        this.haveErrors.firstName = false;
    }

    /**
     * Validates the input of a "Last name" in a field.
     * @param {*} e field input event
     */
    validateLastName(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if(value === '') {
            error.textContent = '';
            return;
        }
        if (!value.match(/^[a-zA-Z,.-]+$/)) {
            error.textContent = 'Invalid char in Last name';
            this.haveErrors.lastName = true;
            return;
        }
        if(value.length > 25) {
            error.textContent = 'First name too long';
            this.haveErrors.lastName = true;
            return;
        }
        if(value.length < 3) {
            error.textContent = 'First name too short';
            this.haveErrors.lastName = true;
            return;
        }
        this.haveErrors.lastName = false;
    }

    /**
     * Validates the input of a "Username" in a field.
     * @param {*} e field input event
     */
    validateUsername(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if (value.match(/[.,\/#!$%\^&\*;:{}=\-`~()]/g)) {
            error.textContent = 'Invalid char in Username';
            this.haveErrors.username = true;
            return;
        }
        if(value.length > 20 || value.length < 5) {
            error.textContent = 'Username must be between 5 and 20 characters';
            this.haveErrors.username = true;
            return;
        }
        this.haveErrors.username = false;
    }

    /**
     * Validates the input of a "E-mail" in a field.
     * @param {*} e field input event
     */
    validateEMail(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if (!value.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            error.textContent = 'Email should look like: xx@yy.zz';
            this.haveErrors.eMail = true;
            return;
        }
        this.haveErrors.eMail = false;
    }

    /**
     * Validates the input of a "Password" in a field.
     * @param {*} e field input event
     */
    validatePassword(e) {
        const value = e.target.value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if (!value.match(/^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{8,20}$/)) {
            error.textContent = 'Password should contain at least one digit and one lower case character and be between 8 and 20 characters';
            this.haveErrors.password = true;
            return;
        }
        this.haveErrors.password = false;
    }

    /**
     * Checks that the entered password matches the one entered earlier.
     * @param {*} e field input event
     */
    validateRePassword(e) {
        const value = e.target.value;
        const originalPassword = document.querySelector('#password').value;
        const labelGroup = e.target.closest('.form__label');
        const error = labelGroup.querySelector('.form__error');
        error.textContent = '';
        if (originalPassword !== value) {
            error.textContent = 'The entered passwords do not match';
            this.haveErrors.rePassword = true;
            return;
        }
        this.haveErrors.rePassword = false;
    }

    /**
     * Checks if the Registration form is correct
     * @param {*} form form
     */
    isRegistrationFormCorrect(form) {
        const firstName = form.querySelector('#first-name').value;
        const lastName = form.querySelector('#last-name').value;
        const username = form.querySelector('#username').value;
        const eMail = form.querySelector('#e-mail').value;
        const password = form.querySelector('#password').value;
        const rePassword = form.querySelector('#repeat-password').value;
        if (!firstName || !lastName || !username || !eMail || !password || !rePassword) {
            form.querySelector('#last-error-form').textContent = 'Please complete all required form fields';
            return false;
        }
        return !this.haveErrors.firstName && !this.haveErrors.lastName && !this.username && !this.haveErrors.eMail && !this.haveErrors.password && !this.haveErrors.rePassword;
    }

    /**
     * Removes all validation handlers from the form.
     */
    removeListenersValidateRegistrationForm() {
        const firstName = document.querySelector('#first-name');
        const lastName = document.querySelector('#last-name');
        const username = document.querySelector('#username');
        const eMail = document.querySelector('#e-mail');
        const password = document.querySelector('#password');
        const rePassword = document.querySelector('#repeat-password');

        firstName.removeEventListener('input', this.validateFirstNameBoundFunction);
        lastName.removeEventListener('input', this.validateLastNameBoundFunction);
        username.removeEventListener('input', this.validateUsernameBoundFunction);
        eMail.removeEventListener('input', this.validateEMailBoundFunction);
        password.removeEventListener('input', this.validatePasswordBoundFunction);
        rePassword.removeEventListener('input', this.validateRePasswordBoundFunction);
    }

    /**
     * Checks if the Login form is correct
     * @param {*} form form
     */
    isLoginFormCorrect(form) {
        const username = form.querySelector('#username').value;
        const password = form.querySelector('#password').value;

        if (!username || !password) {
            form.querySelector('#last-error-form').textContent = 'Please complete all required form fields';
            return false;
        }
        return true;
    }
}

export default Validation;