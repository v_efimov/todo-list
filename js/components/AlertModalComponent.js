'use strict';

class AlertModalComponent {
    constructor() {
        this.alertModal = document.querySelector('.alert-modal');
        this.alertModalTitle = document.querySelector('.alert-modal__title');
        this.alertModalContent = document.querySelector('.alert-modal__content');
        this.alertClose = document.querySelector('.alert-modal__close');

        this.closeBoundFunction = this.close.bind(this);
    }

    /**
     * Opens a modal window and closes after 5 seconds.
     * @param {*} title modal window name
     * @param {*} content modal window content
     */
    open(title, content) {
        this.alertModal.classList.add('alert-modal_is-visible');
        this.alertModal.classList.add('alert-modal_bottom-animation');
        this.alertModalTitle.textContent = title;
        this.alertModalContent.textContent = content;
        this.alertClose.addEventListener('click', this.closeBoundFunction);
        setTimeout(() => {
            this.close()
        }, 5000);  
    }

    /**
     * Opens a modal window without auto-closing.
     * @param {*} title modal window name
     * @param {*} content modal window content
     */
    openWithoutClosing(title, content){
        this.alertModal.classList.add('alert-modal_is-visible');
        this.alertModal.classList.add('alert-modal_bottom-animation');
        this.alertModalTitle.textContent = title;
        this.alertModalContent.textContent = content;
        this.alertClose.addEventListener('click', this.closeBoundFunction);
    }

    /**
     * Closes the modal window.
     */
    close() {
        setTimeout(() => {
            this.alertModal.classList.remove('alert-modal_bottom-animation');
        }, 1000);
        setTimeout(() => {
            this.alertClose.removeEventListener('click', this.closeBoundFunction);
            this.alertModalTitle.textContent = '';
            this.alertModalContent.textContent = '';
            this.alertModal.classList.remove('alert-modal_is-visible');
        }, 1300);
    }
}

export default AlertModalComponent;