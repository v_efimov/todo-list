'use strict';

import AlertModalComponent from './AlertModalComponent.js';
import Filters from './Filters.js';
import LoadMore from './LoadMore.js';
import ProblemComponent from './ProblemComponent.js';

import FilterOptions from '../lib/FilterOptions.js';

import { getData } from '../handlers/api-requests.js';
import { getCookie } from '../handlers/cookies.js';

class ToDo {
    constructor() {
        this.state = {
            userId: -1,
            listState: 'all',
            filterOptions: new FilterOptions(),
            isEndOfData: false
        }
        this.data;
        this.countDisplayedProblems = 0;
        this.todoDiv;
        this.loadMore = new LoadMore();
        this.filters = new Filters(this.state.filterOptions);

        this.changeFilterOptionBoundFunction = this.changeFilterOptionHandler.bind(this);
        this.changeListStateBoundFunction = this.changeListStateHandler.bind(this);
        this.changePaginationBoundFunction = this.changePaginationHandler.bind(this);
        this.createProblemBoundFunction = this.createProblemHandler.bind(this);
        this.changeProblemStatusBoundFunction = this.changeProblemStatus.bind(this);
        this.permanentRemoveProblemBoundFunction = this.permanentRemoveProblemHandler.bind(this);
    }

    /**
     * Checks if the component is rendered.
     * @returns true or false
     */
    isExist() {
        return this.todoDiv ? true : false;
    }

    /**
     * Resets pagination when changing sort options. Loads a new list.
     */
    changeFilterOptionHandler() {
        this.state.filterOptions.page = 1;
        this.countDisplayedProblems = 0;
        this.loadProblems();
    }

    /**
     * Resets pagination when changing list-state. Loads a new list.
     */
    changeListStateHandler() {
        this.state.listState = getCookie('list-state');
        this.state.filterOptions.page = 1;
        this.countDisplayedProblems = 0;
        this.loadProblems();
    }

    /**
     * Increases the page number. Loads a new list.
     */
    changePaginationHandler() {
        this.state.filterOptions.page++;
        this.loadProblems();
    }

    /**
     * Reload the list of problems when a new one is added if list-state is 'active' or 'all'.
     */
    createProblemHandler() {
        if(this.state.listState === 'all' || this.state.listState === 'active') {
            this.state.filterOptions.page = 1;
            this.loadProblems();
        }
    }

    /**
     * Complements the missing problem by changing the status of the next one in the list. 
     * Only when list-state != all.
     */
    changeProblemStatus() {
        if(this.state.listState !== 'all') {
            this.loadXProblemByStatus();
        }
    }

    /**
     * Complements the list with the next problem when one of them is removed.
     */
    permanentRemoveProblemHandler() {
        this.loadXProblemByStatus();
    }

    /**
     * Initializes the component according to the given parameters.
     * @param user - User
     * @param listState - List status
     */
    init(user, listState) {
        this.state.userId = user.id_user;
        this.state.listState = listState;

        document.querySelector('.content').innerHTML=`
            <div class="todo">
                <div class="todo__items items"></div>
                <div class="todo__load-more-button"></div>
            </div>
        `;
        
        this.todoDiv = document.querySelector('.todo');
        this.filters.init(this.state.filterOptions);
        this.loadProblems();

        document.addEventListener('change-filter-option', this.changeFilterOptionBoundFunction);
        document.addEventListener('change-list-state', this.changeListStateBoundFunction);
        document.addEventListener('load-more', this.changePaginationBoundFunction);
        document.addEventListener('problem-created', this.createProblemBoundFunction);
        document.addEventListener('changed-status', this.changeProblemStatusBoundFunction);
        document.addEventListener('permanent-remove', this.permanentRemoveProblemBoundFunction);
    }

    /**
     * Removes the component.
     */
    destroyComponent() {
        this.loadMore.isExist() && this.loadMore.destroyComponent();
        this.filters.isExist() && this.filters.destroyComponent();
        document.removeEventListener('change-filter-option', this.changeFilterOptionBoundFunction);
        document.removeEventListener('change-list-state', this.changeListStateBoundFunction);
        document.removeEventListener('load-more', this.changePaginationBoundFunction);
        document.removeEventListener('problem-created', this.createProblemBoundFunction);
        document.removeEventListener('changed-status', this.changeProblemStatusBoundFunction);
        document.removeEventListener('permanent-remove', this.permanentRemoveProblemBoundFunction);
        this.todoDiv.remove();
    }

    /**
     * Loads and renders a list of problems.
     */
    loadProblems() {
        getData('./api/problem/getProblems.php', `user-id=${this.state.userId}&status=${this.state.listState}&sort=${this.state.filterOptions.sort}&count=${this.state.filterOptions.count}&page=${this.state.filterOptions.page}`).then(
            (response) => {
                this.isEndOfData = JSON.parse(response).data.isEnd;
                if(JSON.parse(response).data.problems.length) {
                    this.data = JSON.parse(response).data.problems;
                    this.renderList();
                    this.loadMore.isExist() || this.loadMore.init('.todo__load-more-button');
                    this.isEndOfData ? this.loadMore.setDisabledStatus() : this.loadMore.removeDisabledStatus();
                } else {
                    if (this.state.filterOptions.page == 1) {
                        document.querySelector('.items').innerHTML = `<div class="items__warning">NO DATA</div>`;
                        this.loadMore.isExist() && this.loadMore.destroyComponent();
                    }
                }
            },
            (error) => {
                const alertModal = new AlertModalComponent();
                alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
            }
        );
    }

    /**
     * Loads and renders a specific problem by status.
     */
    loadXProblemByStatus() {
        getData('./api/problem/getXProblemByStatus.php', 
        `user-id=${this.state.userId}&status=${this.state.listState}&sort=${this.state.filterOptions.sort}&count-displayed=${this.countDisplayedProblems}`).then(
            (response) => {
                this.isEndOfData = JSON.parse(response).data.isEnd;
                if(JSON.parse(response).data.problems.length) {
                    this.data = JSON.parse(response).data.problems;
                    const problem = new ProblemComponent();
                    problem.init('.items', this.data[0], this.state.userId);
                    this.loadMore.isExist() || this.loadMore.init('.todo__load-more-button');
                    this.isEndOfData ? this.loadMore.setDisabledStatus() : this.loadMore.removeDisabledStatus();
                } else {
                    this.countDisplayedProblems--;
                    if (this.countDisplayedProblems === 0) {
                        document.querySelector('.items').innerHTML = `<div class="items__warning">NO DATA</div>`;
                        this.loadMore.isExist() && this.loadMore.destroyComponent();
                    }
                }
            },
            (error) => {
                const alertModal = new AlertModalComponent();
                alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
            }
        );
    }

    /**
     * Renders a list of problems.
     */
    renderList() {
        if (this.state.filterOptions.page == 1) {
            document.querySelector('.items').innerHTML = ``;
        }
        this.data.forEach(item => {
            const problem = new ProblemComponent();
            problem.init('.items', item, this.state.userId);
            this.countDisplayedProblems++;
        });
    }
}

export default ToDo;