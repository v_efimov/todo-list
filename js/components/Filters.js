'use strict';

class Filters {
    constructor(options) {
        this.state = options;
        this.filters;
        this.changeOptionHandler = this.changeOption.bind(this);
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return document.querySelector('.filters') ? true : false;
    }

    /**
     * Initializes the component.
     * @param {*} options Filter Options
     */
    init(options) {
        this.state = options;
        if(!document.querySelector('.filters')) {
            document.querySelector('.todo').insertAdjacentHTML('afterbegin', `
                <div class="todo__filters filters">
                    <div class="filters__filter" id="sort-by-date">
                        <span class="filters__descr">Sorting by date:</span>
                        <div class="filters__option" id="up-to-down">
                            <svg class="filters__option-icon">
                                <use xlink:href="./assets/icons/icons.svg#arrow"></use>
                            </svg>
                        </div>
                        <div class="filters__option" id="down-to-up">
                            <svg class="filters__option-icon filters__option-icon_reversed">
                                <use xlink:href="./assets/icons/icons.svg#arrow"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="filters__filter" id="pagination">
                        <span class="filters__descr">Display problems by:</span>
                        <div class="filters__option" id="5">5</div>
                        <div class="filters__option" id="10">10</div>
                        <div class="filters__option" id="15">15</div>
                    </div>
                </div>
            `);
        }

        this.filters = document.querySelector('.filters');

        document.querySelectorAll('.filters__option').forEach(el => {
            el.addEventListener('click', this.changeOptionHandler);
            if (el.id === this.state.sort || el.id === this.state.count) {
                el.classList.add('filters__option_checked');
            }
        });
    }


    /**
     * Changes filtering options
     * @param {*} event event
     * @returns 
     */
    changeOption(event) {
        const element = event.target;
        const filter = element.closest('.filters__filter');
        if(element.classList.contains('filters__option_checked')) {
            return;
        } else {
            this.setActiveOption(filter, element);
        }
        if (filter.id === 'sort-by-date') {
            this.state.sort = element.id;
            document.dispatchEvent(new Event('change-filter-option'));
        }
        if (filter.id === 'pagination') {
            this.state.count = element.id;
            document.dispatchEvent(new Event('change-filter-option'));
        }
    }

    /**
     * Sets the active state to an element in a filter.
     * @param {*} filter filter
     * @param {*} element element
     */
    setActiveOption(filter, element) {
        filter.querySelectorAll('.filters__option').forEach(el => {
            el.classList.remove('filters__option_checked');
        })
        element.classList.add('filters__option_checked');
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        document.querySelectorAll('.filters__option').forEach(el => {
            el.removeEventListener('click', this.changeOptionHandler);
        });
        this.filters.remove();
    }
}

export default Filters;