'use strict';

class LoadMore {
    constructor() {
        this.loadMoreButton;
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.loadMoreButton ? true : false;
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     */
    init(location) {
        this.loadMoreButton = document.createElement('button');
        this.loadMoreButton.className = 'button button_primary';
        this.loadMoreButton.id = 'load-more';
        this.loadMoreButton.textContent = 'Load More';
        document.querySelector(location).append(this.loadMoreButton);
        this.loadMoreButton.addEventListener('click', this.loadMoreClickHandler);
    }

    /**
     * Raises the button click event LoadMore.
     */
    loadMoreClickHandler() {
        document.dispatchEvent(new Event('load-more'));
    }

    /**
     * Sets disable status.
     */
    setDisabledStatus() {
        this.loadMoreButton.classList.add('button_disabled');
        this.loadMoreButton.setAttribute('disabled', '');
    }

    /**
     * Removes disable status.
     */
    removeDisabledStatus() {
        this.loadMoreButton.classList.remove('button_disabled');
        this.loadMoreButton.removeAttribute('disabled', '');
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.loadMoreButton.removeEventListener('click', this.loadMoreClickHandler);
        this.loadMoreButton.remove();
        this.loadMoreButton = undefined;
    }
}

export default LoadMore;