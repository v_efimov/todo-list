'use strict';

import AlertModalComponent from './AlertModalComponent.js';
import InputError from './InputError.js';
import Popup from './Popup.js';
import Validation from './Validation.js';

import { postData } from '../handlers/api-requests.js';
import { resetAllErrorsInForm } from '../handlers/others.js';

class Registration {
    constructor() {
        this.popup = new Popup();
        this.validate = new Validation();
        this.registrationButton;
        this.registrationForm;
        this.openRegistrationFormBoundFunction = this.openRegistrationForm.bind(this);
        this.registrationFormHandlerBoundFunction = this.registrationFormHandler.bind(this);
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.registrationButton ? true : false; 
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     */
    init(location) {
        const button = document.createElement('button'); //TODO: Create BaseClass Button?
        button.className = 'button button_secondary';
        button.id = 'registration';
        button.textContent = 'Registration';
        document.querySelector(location).append(button);
        this.registrationButton = document.querySelector('#registration');
        this.registrationButton.addEventListener('click', this.openRegistrationFormBoundFunction);
    }

    /**
     * Opens the registration form.
     */
    openRegistrationForm() {
        this.popup.open(this.popup.createRegistrationForm());
        this.registrationForm = document.querySelector('#registration-form');
        this.validate.validateRegistrationForm();
        this.registrationForm.addEventListener('submit', this.registrationFormHandlerBoundFunction);
    }

    /**
     * Handles submitting the registration form.
     * @param {*} e event
     */
    registrationFormHandler(e) {
        e.preventDefault();
        const firstName = this.registrationForm.querySelector('#first-name').value;
        const lastName = this.registrationForm.querySelector('#last-name').value;
        const username = this.registrationForm.querySelector('#username').value;
        const eMail = this.registrationForm.querySelector('#e-mail').value;
        const password = this.registrationForm.querySelector('#password').value;
        const repeatPassword = this.registrationForm.querySelector('#repeat-password').value;
        
        if(this.validate.isRegistrationFormCorrect(this.registrationForm)) {
            postData('./api/user/registration.php', 
            `firstName=${firstName}&lastName=${lastName}&username=${username}&eMail=${eMail}&password=${password}&repeatPassword=${repeatPassword}`).then(
                (response) => {
                    const responseFromBack = JSON.parse(response);
                    if(responseFromBack.type === 'VALIDATION-ERROR') {
                        resetAllErrorsInForm(this.registrationForm);
                        const errors = responseFromBack.data;
                        errors.forEach(el => {
                            const error = new InputError(el);
                            error.show(this.registrationForm);
                        });
                        this.registrationForm.querySelector('#password').value = '';
                        this.registrationForm.querySelector('#repeat-password').value = '';
                    } 
                    if(responseFromBack.type === 'response') {
                        const popup = new Popup();
                        this.registrationForm.removeEventListener('submit', this.registrationFormHandlerBoundFunction);
                        this.validate.removeListenersValidateRegistrationForm();
                        popup.close();
                        const alertModal = new AlertModalComponent();
                        alertModal.open('Success!', `User ${username} successfully registered. Now you can login to the app.`);
                    }
                },
                (error) => {
                    const alertModal = new AlertModalComponent();
                    alertModal.open(`${error.name} ${error.code}`, error.message);
                }
            ); 
        }
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.registrationButton.removeEventListener('click', this.openRegistrationFormBoundFunction);
        this.registrationButton.remove();
    }
}

export default Registration;