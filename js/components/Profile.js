'use strict';

import Logout from "./Logout.js";

class Profile {
    constructor() {
        this.profileElement;
        this.logout = new Logout();
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.profileElement ? true : false;
    }

    /**
     * Initializes the component.
     * @param {*} location the place where the component will be inserted
     * @param {*} firstName First username
     * @param {*} lastName Last username
     */
    init(location ,firstName, lastName) {
        document.querySelector(location).innerHTML = `
            <div class="profile">
                <a href="#" class="profile__avatar">
                    <img src="./assets/images/avatar.jpg" alt="User's photo" >
                </a>
                <div class="profile__info">
                    <a href="#" class="profile__name">${firstName} ${lastName}</a>
                </div>
            </div>
        `;
        this.logout.init();
        this.profileElement = document.querySelector('.profile');
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.logout.destroyComponent();
        this.profileElement.remove();
    }
}

export default Profile;