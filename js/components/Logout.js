'use strict';

import AlertModalComponent from './AlertModalComponent.js';

import { deleteCookie } from '../handlers/cookies.js';

class Logout {
    constructor() {
        this.logoutButton;
        this.logoutHandler;
    }

    /**
     * Initializes the component.
     */
    init() {
        const button = document.createElement('button');
        button.classList = 'profile__exit button button_primary button_small';
        button.id = 'logout';
        button.textContent = 'Logout';
        document.querySelector('.profile__info').append(button);
        this.logoutButton = document.querySelector('#logout');
        this.logoutButton.addEventListener('click', this.logOut);
    }

    /**
     * Logs out the user.
     */
    logOut() {
        deleteCookie('auth');
        deleteCookie('csrf-token');
        const alertModal = new AlertModalComponent();
        alertModal.open('Success!', 'The application exited successfully!');
        document.dispatchEvent(new Event('logout'));
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.logoutButton.removeEventListener('click', this.logOut);
        this.logoutButton.remove();
    }
}

export default Logout;