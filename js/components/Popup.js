'use strict';

class Popup {
    constructor() {
        this.popup = document.querySelector('.popup');
        this.popupContent = document.querySelector('.popup__content');
        this.body = document.querySelector('body');
        this.clickOnClose = this.clickOnClosePopup.bind(this);
        this.mouseDownOutside = this.mouseDownOutsidePopup.bind(this);
    }

    /**
     * Opens a popup with content.
     * @param {*} content content
     */
    open(content) {
        this.popup.classList.add('popup_is-visible');
        this.body.classList.add('noscroll');
        this.popupContent.innerHTML = content;
        this.popup.addEventListener('click', this.clickOnClose);
        this.popup.addEventListener('mousedown', this.mouseDownOutside);
    }

    /**
     * Closes the popup.
     */
    close() {
        this.popup.classList.remove('popup_is-visible');
        this.body.classList.remove('noscroll');
        this.popup.removeEventListener('click', this.clickOnClose);
        this.popup.removeEventListener('mousedown', this.mouseDownOutside);
        setTimeout(() => {this.popupContent.innerHTML = ''}, 300);  //For correct transition-animation
    }

    /**
     * Handles the click event on "close popup".
     * @param {*} e event
     */
    clickOnClosePopup(e) {
        if (e.target.classList.contains('popup__close-btn')) {
            e.preventDefault();
            this.close();
        }
    }

    /**
     * Handles the mousedown event outside the popup.
     * @param {*} e event
     */
    mouseDownOutsidePopup(e) {
        if (!e.composedPath().includes(document.querySelector('.popup__body'))) {
            e.preventDefault();
            this.close();
        }
    }

    /**
     * Prepares the layout for the form to create a problem.
     * @returns layout
     */
    createAddProblemForm() {
        return `
        <form class="form" id="create-problem-form">
            <h2 class="form__title">New problem</h2>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Name</span>
                    <input type="text" class="form__input" id="name" name="name" required>
                    <span class="form__error"></span>
                </label>
            </div>
            <div class="form__group">
                <label class="form__label">
                    <span class="form__label-name">Priority</span>
                    <select class="form__input form__input_small" id="priority" name="priority">
                        <option>Low</option>
                        <option selected>Normal</option>
                        <option>High</option>
                    </select>
                    <span class="form__error"></span>
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Date</span>
                    <input type="date" id="date" name="date" class="form__input form__input_small" pattern="(?:19|20)\[0-9\]{2}-(?:(?:0\[1-9\]|1\[0-2\])-(?:0\[1-9\]|1\[0-9\]|2\[0-9\])|(?:(?!02)(?:0\[1-9\]|1\[0-2\])-(?:30))|(?:(?:0\[13578\]|1\[02\])-31))"  required> 
                    <span class="form__error"></span> 
                </label>
            </div>
            <div class="form__group">
                <label class="form__label">
                    <span class="form__label-name">Description</span>
                    <textarea class="form__input_textarea" name="description" id="description" placeholder="Enter description here..."></textarea>
                    <span class="form__error" id="last-error-form"></span> 
                </label>
            </div>
            <button class="form__submit button button_primary" type="submit" form="create-problem-form" value="Submit" id="submit-btn">Submit</button>
        </form>
        `;
    }

    /**
     * Prepares a layout for detailed information about the problem.
     * @param {*} problem problem
     * @returns layout
     */
    createInfoElement(problem) {
        return `
        <div class="full-info-item">
            <h2 class="full-info-item__title">${problem.name}</h2>
            <div class="full-info-item__group">
                <span class="full-info-item__subtitle">Priority: </span>
                <span class="full-info-item__priority ${problem.priority === 'High' && 'full-info-item__priority_high'}">${problem.priority}</span>
            </div>
            <div class="full-info-item__group">
                <span class="full-info-item__subtitle">Status: </span>
                <span 
                    class="full-info-item__status
                    ${problem.status === 'completed' && 'full-info-item__status_completed'}
                    ${problem.status === 'deleted' && 'full-info-item__status_deleted'}
                ">
                    ${problem.status}
                </span>
            </div>
            <div class="full-info-item__group">
                <span class="full-info-item__subtitle">Date: </span>
                <span>${problem.date}</span>
            </div>
            <div class="full-info-item__group full-info-item__group_column">
                <span class="full-info-item__subtitle">Description:</span>
                <span>${problem.description || '*NO DESCRIPTION*'}</span>
            </div>
        </div>
        `;
    }

    /**
     * Prepares the layout for the registration form.
     * @returns layout
     */
    createRegistrationForm() {
        return `
        <form class="form form_registration" id="registration-form">
            <h2 class="form__title">Registration</h2>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">First name</span>
                    <input type="text" class="form__input" id="first-name" name="first-name" required>
                    <span class="form__error"></span>
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Last name</span>
                    <input type="text" class="form__input" id="last-name" name="last-name" required>  
                    <span class="form__error"></span>
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Username</span>
                    <input type="text" class="form__input" id="username" name="username" required> 
                    <span class="form__error"></span> 
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">E-mail</span>
                    <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="form__input" id="e-mail" name="e-mail" required>
                    <span class="form__error"></span>  
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Password</span>
                    <input type="password" class="form__input" id="password" name="password" required>
                    <span class="form__error"></span>  
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Repeat Password</span>
                    <input type="password" class="form__input" id="repeat-password" name="repeat-password" required>
                    <span class="form__error" id="last-error-form"></span>  
                </label>
            </div>
            <button class="form__submit button button_primary" type="submit" form="registration-form" value="Submit" id="submit-btn">Registration</button>
        </form>
        `;
    }

    /**
     * Prepares the layout for the login form.
     * @returns layout
     */
    createLoginForm() {
        return `
        <form class="form" id="login-form">
            <h2 class="form__title">Login</h2>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Username</span>
                    <input type="text" class="form__input" id="username" name="username">
                    <span class="form__error"></span>  
                </label>
            </div>
            <div class="form__group">
                <label class="form__label form__label_required">
                    <span class="form__label-name">Password</span>
                    <input type="password" class="form__input" id="password" name="password">
                    <span class="form__error" id="last-error-form"></span>  
                </label>
            </div>
            <button class="form__submit button button_primary" type="submit" form="login-form" value="Submit" id="submit-btn">Login</button>
        </form>
        `;
    }
}

export default Popup;