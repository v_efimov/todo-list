'use strict';

import AlertModalComponent from './AlertModalComponent.js';
import InputError from './InputError.js';
import Popup from './Popup.js';
import Validation from './Validation.js';

import { postData } from '../handlers/api-requests.js';
import { resetAllErrorsInForm } from '../handlers/others.js';

class Login {
    constructor() {
        this.popup = new Popup();
        this.alertModal = new AlertModalComponent();
        this.validate = new Validation();
        this.loginButton;
        this.loginForm;
        this.openLoginFormBoundFunction = this.openLoginForm.bind(this);
        this.loginFormHandlerBoundFunction = this.loginFormHandler.bind(this);
    }

    /**
     * Checks if the component has been initialized.
     * @returns true or false
     */
    isExist() {
        return this.loginButton ? true : false;
    }

    /**
     * Initializes the component.
     * @param {*} location The place where the component will be inserted
     */
    init(location) {
        const button = document.createElement('button');
        button.className = 'button button_primary';
        button.id = 'login';
        button.textContent = 'Login';
        document.querySelector(location).append(button);
        this.loginButton = document.querySelector('#login');
        this.loginButton.addEventListener('click', this.openLoginFormBoundFunction);
    }

    /**
     * Opens the login form.
     */
    openLoginForm() {
        this.popup.open(this.popup.createLoginForm());
        this.loginForm = document.querySelector('#login-form');
        this.loginForm.addEventListener('submit', this.loginFormHandlerBoundFunction);
    }

    /**
     * Handles submitting the login form.
     * @param {*} e event
     */
    loginFormHandler(e) {
        e.preventDefault();
        const form = document.querySelector('#login-form');
        const username = form.querySelector('#username').value.trim();
        const password = form.querySelector('#password').value.trim();
        resetAllErrorsInForm(this.loginForm);
        if(this.validate.isLoginFormCorrect(this.loginForm)) {
            postData('./api/user/login.php', `username=${username}&password=${password}`).then(
                (response) => {
                    const responseFromBack = JSON.parse(response);
                    if(responseFromBack.type === 'VALIDATION-ERROR') {
                        const errors = responseFromBack.data;
                        errors.forEach(el => {
                            const error = new InputError(el);
                            error.show(this.loginForm);
                        });
                        form.querySelector('#password').value = '';
                    }
                    if(responseFromBack.type === 'response') {
                        document.dispatchEvent(new Event('login'));
                        this.loginForm.removeEventListener('submit', this.loginFormHandlerBoundFunction);
                        this.popup.close();
                        this.alertModal.open('Success!', 'Login to the application was successful!');
                    }
                },
                (error) => {
                    const alertModal = new AlertModalComponent();
                    alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
                }
            );
        }
    }

    /**
     * Destroys the component.
     */
    destroyComponent() {
        this.loginButton.removeEventListener('click', this.openLoginFormBoundFunction);
        this.loginButton.remove();
    }
}

export default Login;