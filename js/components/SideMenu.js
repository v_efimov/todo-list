'use strict';

import SwitcherTheme from "./SwitcherTheme.js";

import { getCookie, setCookie } from '../handlers/cookies.js';

class SideMenu {
    constructor() {
        this.switcherTheme = new SwitcherTheme();
        this.switcherTheme.init('.side-bar');
        this.listState = getCookie('list-state') ? getCookie('list-state') : 'all';
        this.button = document.querySelector('.side-bar__burger');
        this.sideBar =  document.querySelector('.side-bar');
        this.body = document.querySelector('body');

        document.querySelectorAll('.menu-point').forEach(point => {
            if(point.id === this.listState) {
                this.setActivePointStatus(point);
            }
        })

        this.button.addEventListener('click', (e) => {
            this.sideBar.classList.toggle('side-bar_is-expanded');
            this.body.classList.toggle('noscroll');

            document.querySelectorAll('.menu-point').
            forEach(point => point.addEventListener('click',() => { 
                this.sideBar.classList.remove('side-bar_is-expanded'); 
                this.body.classList.remove('noscroll');
            }));
        });
        
        document.querySelectorAll('.menu-point').forEach(point => point.addEventListener('click', this.changeListState.bind(this)));
    }

    /**
     * Raises an event about a change in the state of the problem list.
     * @param {*} e an event on an element that changes state
     */
    changeListState(e) {
        e.stopPropagation();
        if(e.target.classList.contains('menu-point_active')) {
            return;
        }
        setCookie('list-state', e.target.id, 1);
        document.dispatchEvent(new Event('change-list-state'));
        this.setActivePointStatus(e.target);
    }

    /**
     * Sets the status of an element to Active
     * @param {*} element element
     */
    setActivePointStatus(element) {
        document.querySelectorAll('.menu-point').forEach(item => item.classList.remove('menu-point_active'));
        element.classList.add('menu-point_active');
    }
}

export default SideMenu;