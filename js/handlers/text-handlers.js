/**
 * Limits the output of text in Node to the number of characters
 * @param {*} location Node where the constraint is needed
 * @param {*} countOfChar Characters
 */
export function textOverflowInNode(location ,countOfChar) {
    if (location.innerHTML.length > countOfChar) {
        location.innerHTML = location.innerHTML.slice(0, countOfChar) + "...";
    }
}
