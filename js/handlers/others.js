/**
 * Reloads all error fields in the form.
 * @param {*} form form
 */
export function resetAllErrorsInForm(form) { //TODO: Think where to put it
    form.querySelectorAll('.form__error').forEach(e => {
        e.textContent = '';
        e.classList.remove('form__error_shake');
    });
}