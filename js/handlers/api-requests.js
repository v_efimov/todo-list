/**
 * Generates a GET request to the server.
 * @param {*} url request url
 * @param {*} params request parameters
 * @returns server response
 */
export function getData(url, params = '') {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", `${url}?${params}`, true);

        xhr.onload = function () {
            if (this.status === 200) {
                if (!isJsonString(this.response) && this.response) {
                    const error = errorParse(this.response);
                    reject(error);
                }
                resolve(this.response);
            } else {
                const error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Error"));
        };

        xhr.send();
    });
}

/**
 * Generates a POST request to the server.
 * @param {*} url request url
 * @param {*} params request parameters
 * @returns server response
 */
export function postData(url, params) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);

        xhr.onload = function () {
            if (this.status === 200 ) {
                if (!isJsonString(this.response) && this.response) {
                    const error = errorParse(this.response);
                    reject(error);
                }
                resolve(this.response);
            } else {
                const error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Error"));
        };

        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(params);
    });
}

/**
 * Checks a string for JSON format.
 * @param {*} str input string
 */
function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * Parses the error received from the PHP server.
 * @param {*} error error
 * @returns an instance of the Error class, or an unparsed error if not in PHP-error format
 */
function errorParse(error) {
    if (error.indexOf('<br />') != -1) {
        error = error.substring('<br />'.length);
        error = error.split('<br />')[0];
        const title = error.slice(error.indexOf('<b>') + '<b>'.length, error.indexOf('</b>'));
        let msg = error.slice(error.indexOf('</b>') + '</b>'.length + 1);
        msg = msg.replace(/(<([^>]+)>)/ig, '');
        const errorObj = new Error();
        errorObj.name = title;
        errorObj.message = msg;
        return errorObj;
    } else {
        return error;
    }
}