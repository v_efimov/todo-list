/**
 * Gets cookie.
 * @param {*} cookieName cookie name
 */
export function getCookie(cookieName){
    const setOfCookies = document.cookie.split(";");
    for (let i = 0; i < setOfCookies.length; i++) {
        const cookie = setOfCookies[i];

        const inProgressCookie = cookie.split("=");
        if(inProgressCookie[0].trim() === cookieName)
            return inProgressCookie[1].trim();
    }

    return false;
}

/**
 * Gets cookie.
 * @param {*} cookieName cookie name
 * @param {*} cookieValue cookie value
 * @param {*} expirationDays cookie expiration in days
 */
export function setCookie(cookieName, cookieValue, expirationDays){
    const d= new Date();
    d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
    document.cookie = cookieName + "=" + cookieValue + ";" + "expires=" + d.toUTCString() + ";";
}

/**
 * Deletes a cookie by name.
 * @param {*} name cookie name
 */
export function deleteCookie(name) {
    const cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        const cookie = cookies[i];
        const eqPos = cookie.indexOf("=");
        let nameCookie = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
        nameCookie = nameCookie.trim()
        if (nameCookie === name) {
            document.cookie = nameCookie + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
}

/**
 * Deletes all cookies.
 */
export function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}