export default class FilterOptions {
    constructor(count = '5', sort = 'up-to-down', page = 1) {
        this.count = count;
        this.sort = sort;
        this.page = page
    }
}
