export default class Problem {
    constructor(name, date, description, priority = 'Normal') {
        this.name = name;
        this.date = date;
        this.description = description;
        this.priority = priority;
        this.status = 'active';
    }
}
