'use strict';

import AlertModalComponent from './components/AlertModalComponent.js';
import Header from './components/Header.js';
import SideMenu from './components/SideMenu.js';
import ToDo from './components/ToDo.js';

import User from './lib/User.js';

import { getData } from './handlers/api-requests.js';
import { getCookie, setCookie } from './handlers/cookies.js';

class App {
    constructor() {
        this.state = {
            isAuthenticated: getCookie('auth') ? true : false,
            user: new User(),
            listState: getCookie('list-state') ? getCookie('list-state') : 'all'
        }
        this.header = new Header();
        this.menu = new SideMenu();
        this.todo = new ToDo();
        
        this.loginBoundFunction = this.login.bind(this);
        this.logoutBoundFunction = this.logout.bind(this);
        this.state.isAuthenticated ? document.addEventListener('logout', this.logoutBoundFunction) : document.addEventListener('login', this.loginBoundFunction);
    }

    /**
     * Initializes the component.
     */
    init() {
        if (this.state.isAuthenticated) {
            getData('./api/user/getUser.php').then(
                (response) => {
                    const responseFromBack = JSON.parse(response);
                    if(responseFromBack.type === 'response') {
                        this.state.user = responseFromBack.data.user;
                        setCookie('auth', true, 1);
                        setCookie('csrf-token', responseFromBack.data.csrf_token, 0.01);
                        this.header.createHeaderForAuthorizedUser(this.state.user);
                        this.todo.init(this.state.user, this.state.listState); 
                    }
                },
                (error) => {
                    const alertModal = new AlertModalComponent();
                    alertModal.openWithoutClosing(`${error.name} ${error.code || ''}`, error.message);
                }
            )
        } else {
            this.needToLoginWarning();
            this.header.createHeaderForUnauthorizedUser();
        }
    }

    /**
     * Handles the login event.
     */
    login() {
        this.state.isAuthenticated = true;
        document.removeEventListener('login', this.loginBoundFunction);
        document.addEventListener('logout', this.logoutBoundFunction);
        this.init();
    }

    /**
     * Handles the logout event.
     */
    logout() {
        this.state.isAuthenticated = false;
        document.removeEventListener('logout', this.logoutBoundFunction);
        document.addEventListener('login', this.loginBoundFunction);
        this.init();
    }

    /**
     * Clears the content space and displays a message about the need for authorization.
     */
    needToLoginWarning() {
        this.todo.isExist() && this.todo.destroyComponent();
        document.querySelector('.content').innerHTML =`<div class="content__warning">You need to login or register to use the app</div>`;
    }
}

const app = new App();
app.init();
