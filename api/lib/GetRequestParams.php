<?php
    
    /**
     * Class for parsing get requests for problems.
     */
    class GetRequestParams {        
        /**
         * User ID
         * @var mixed
         */
        public $userId;        
        /**
         * List-state status.
         * @var mixed
         */
        public $status;        
        /**
         * Sort type.
         * @var mixed
         */
        public $sortType;        
        /**
         * Count Problems.
         * @var mixed
         */
        public $countProblems;        
        /**
         * Page.
         * @var mixed
         */
        public $page;
    
        public function __construct(string $userId, string $status, string $sortType,
            string $countProblems, string $page) {
            $this->userId = $userId;
            $this->status = $status;
            $this->sortType = $sortType;
            $this->countProblems = $countProblems;
            $this->page = $page;
        }
    }
?>