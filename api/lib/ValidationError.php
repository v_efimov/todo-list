<?php
    /**
     * Class for defining a validation error.
     */
    class ValidationError {         
        /**
         * Error type.
         * @var string
         */
        public $type = "VALIDATION-ERROR";        
        /**
         * Error Message.
         * @var string
         */
        public $message = '';        
        /**
         * Error location.
         * @var string
         */
        public $location = '';
    }
?>