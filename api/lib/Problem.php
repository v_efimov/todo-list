<?php
    include_once '../helpers/sendingAnswer.php';
        
    /**
     * PHP class Problem for ToDoList App.
     */
    class Problem {        
        /**
         * Problem's ID.
         * @var mixed
         */
        public $id;        
        /**
         * Problem's name.
         * @var mixed
         */
        public $name;        
        /**
         * Problem's description.
         * @var mixed
         */
        public $description;        
        /**
         * Problem's priority.
         * @var mixed
         */
        public $priority;        
        /**
         * Problem's date.
         * @var mixed
         */
        public $date;        
        /**
         * Problem's status.
         * @var mixed
         */
        public $status;
    
        public function __construct($id = null) {
            if(func_num_args() > 1){
                $this->id = rand(0, 100000);
                $this->name = func_get_arg(0);
                $this->description = func_get_arg(1);
                $this->priority = func_get_arg(2);
                $this->date = func_get_arg(3);
                $this->status = func_get_arg(4);
            } else {
                $this->id = $id; 
            }
        }

        /**
         * Adds a task for the user to the database.
         * @param  mixed $connect connect to DB
         * @param  mixed $userId User ID
         * @return void
         */
        public function addProblemToDB($connect, $userId) {
            $sql = "INSERT INTO `problems` VALUES ('$userId', '$this->id', '$this->name', '$this->description', '$this->priority', '$this->date', '$this->status')";
            $execute = mysqli_query($connect, $sql);
            if (!$execute) {
                http_response_code(500);
                exit();
            }
        }

        /**
         * Changes the problem status for the given user in the database.
         * @param  mixed $newStatus new Problem status
         * @param  mixed $userId User ID
         * @param  mixed $connect connect to DB
         * @return void
         */
        public function changeProblemStatusInDB($newStatus, $userId, $connect) {
            $sql = "UPDATE `problems` SET `status` = '$newStatus' WHERE `id_user` = '$userId' AND `id_problem` = '$this->id'";
            $execute = mysqli_query($connect, $sql); 
            if (!$execute) {
                http_response_code(500);
                exit();
            } 
        }
        
        /**
         * Removes the problem for the given user in the database.
         * @param  mixed $userId User ID
         * @param  mixed $connect connect to DB
         * @return void
         */
        public function deleteProblemFromDB($userId, $connect) {
            $sql = "DELETE FROM `problems` WHERE `id_user` = '$userId' AND `id_problem` = '$this->id'";
            $execute = mysqli_query($connect, $sql);
            if (!$execute) {
                http_response_code(500);
                exit();
            } 
        }

        /**
         * Gets complete information about the problem from the database.
         * @param  mixed $connect connect to DB
         * @return void
         */
        public function getProblemFullInfoFromDB($connect) {
            $sql = "SELECT * FROM `problems` WHERE `id_problem` = '$this->id'";
            $execute = mysqli_query($connect, $sql);
            if (mysqli_num_rows($execute) > 0) {
                $problem = mysqli_fetch_assoc($execute);
                sendAnswerWithData($problem);
            } else {
                http_response_code(404);
                exit();
            }
        }
    }
?>
