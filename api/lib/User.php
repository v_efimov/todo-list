<?php 
    /**
     * PHP class User for ToDoList App.
     */
    class User {        
        /**
         * User's ID.
         * @var mixed
         */
        public $id;        
        /**
         * User firstName.
         * @var mixed
         */
        public $firstName;        
        /**
         * User's lastName.
         * @var mixed
         */
        public $lastName;        
        /**
         * User's username.
         * @var mixed
         */
        public $username;        
        /**
         * User's E-mail.
         * @var mixed
         */
        public $eMail;        
        /**
         * User's password.
         * @var mixed
         */
        public $password;

        public function __construct($id = null) {
            if(func_num_args() > 1){
                $this->id = rand(0, 100000);
                $this->firstName = func_get_arg(0);
                $this->lastName = func_get_arg(1);
                $this->username = func_get_arg(2);
                $this->eMail = func_get_arg(3);
                $this->password = password_hash(func_get_arg(4), PASSWORD_DEFAULT);
            } else {
                $this->id = $id; 
            }
        }
   
        /**
         * Adds a user to the database.
         * @param  mixed $connect connect to DB
         * @return void 
         */
        public function addUserToDB($connect) {
            $sql = "INSERT INTO `users` VALUES ('$this->id', '$this->firstName', '$this->lastName', '$this->username', '$this->eMail', '$this->password')";
            $execute = mysqli_query($connect, $sql);
            if (!$execute) {
                http_response_code(500);
                exit();
            }
        }
        
        /**
         * Gets user information from the database. Adds a CSRF token to the response.
         * @param  mixed $connect connect to DB
         * @return void
         */
        public function getUserFromDBWithToken($connect) {  
            $findUser = mysqli_query($connect, "SELECT * FROM `users` WHERE `id_user` = '$this->id'");
            
            if(mysqli_num_rows($findUser) > 0) {
                $user = mysqli_fetch_assoc($findUser);
                unset($user['password']);
                $token = md5(uniqid(rand(), true));
                $_SESSION['csrf-token'] = $token;
                $responseToRequest = [
                    'csrf_token' => $token,
                    'user' => $user
                ];
                return $responseToRequest;
            } else {
                http_response_code(404);
                exit();
            }
        }
    }
    
    /**
     * Finds out if the user with the given username and password can enter the application.
     * @param  mixed $connect connect to DB
     * @param  mixed $username User's username
     * @param  mixed $password User's password
     * @return boolean
     */
    function isUserLogged($connect, $username, $password) {
        //TODO: Should this method be inside the class User?
        $checkUser = mysqli_query($connect, "SELECT * FROM `users` WHERE `username` = '$username'");
        
        if(mysqli_num_rows($checkUser) > 0) {
            $user = mysqli_fetch_assoc($checkUser);
            if(password_verify($password, $user['password'])) {
                $_SESSION['user'] = $user['id_user'];
                return true;
            }
        }
        
        return false;
    }
?>