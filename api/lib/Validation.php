<?php
	include_once 'ValidationError.php';
	include_once '../db/connect.php';
	include_once '../helpers/others.php';
	include_once '../helpers/csrf.php';
	include_once '../helpers/sendingAnswer.php';

	if(!isset($_SESSION)) { 
        session_start(); 
    }
		
	/**
	 * Simple PHP class for validation.
	 */
	class Validation {     
        /**
		 * Validation Patterns.
         * @var array $patterns
         */
        public $patterns = array(
            'pattern_name'        => '/^[a-z ,.-]{3,25}+$/i',
            'pattern_mail'        => '/\w+@\w+\.\w+/',
            'pattern_username'    => '/^[a-z\d_]{5,20}$/i',
            'pattern_password'    => '/^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{8,20}$/',
            'pattern_problemName' => '/[.,\/#!$%\^&\*;:{}=\-_`~()]/i'
        );
        
        /**
		 * Storage for validation errors.
         * @var array $errors
         */        
        public $errors = array();

		/**
		 * Validates the User first name. If there are errors, write them in the class field $errors.
		 * @param  mixed $firstName User first name
		 * @return void
		 */
		private function validateFirstName($firstName) {
			$error = new ValidationError();
			$error->location = "first-name";
			if (empty($firstName)) {
				$error->message = "Enter First name";
			} else {
				if (!preg_match($this->patterns['pattern_name'], $firstName)) {
					$error->message = "Invalid characters in First name or the First name must be from 3 to 25 characters";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the User last name. If there are errors, write them in the class field $errors.
		 * @param  mixed $lastName User last name
		 * @return void
		 */
		private function validateLastName($lastName) {
			$error = new ValidationError();
			$error->location = "last-name";
			if (empty($lastName)) {
				$error->message = "Enter Last name";
			} else {
				if (!preg_match($this->patterns['pattern_name'], $lastName)) {
					$error->message = "Invalid characters in Last name or the Last name must be from 3 to 25 characters";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the Username. If there are errors, write them in the class field $errors.
		 * @param  mixed $username Username
		 * @return void
		 */
		private function validateUsername($username) {
			$error = new ValidationError();
			$error->location = "username";
			if (empty($username)) {
				$error->message = "Enter Username";
			} else {
				if (!preg_match($this->patterns['pattern_username'], $username)) {
					$error->message = "Username must be between 5 and 20 characters long and not contain any punctuation marks";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the User E-mail. If there are errors, write them in the class field $errors.
		 * @param  mixed $eMail User E-mail
		 * @return void
		 */
		private function validateEMail($eMail) {
			$error = new ValidationError();
			$error->location = "e-mail";
			if (empty($eMail)) {
				$error->message = "Enter E-mail";
			} else {
				if (!preg_match($this->patterns['pattern_mail'], $eMail)) {
					$error->message = "Email should look like: xx@yy.zz";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Verifies that the password satisfies all requirements and matches the re-entered password. If there are errors, write them in the class field $errors.
		 * @param  mixed $password password
		 * @param  mixed $repeatPassword re-entered password
		 * @return void
		 */
		private function validatePasswords($password, $repeatPassword) {		
			if (empty($password)) {
				$error = new ValidationError();
				$error->message = "Enter Password";
				$error->location = "password";
				$this->errors[] = $error;
			} else {
				if (!preg_match($this->patterns['pattern_password'], $password)) {
					$error = new ValidationError();
					$error->location = "password";
					$error->message = "Password should contain at least one digit and one lower case character and be between 8 and 20 characters";
					$this->errors[] = $error;
				}
			}
			if (empty($repeatPassword)) {
				$error = new ValidationError();
				$error->message = "Confirm password";
				$error->location = "repeat-password";
				$this->errors[] = $error;
			}
			if ($password != $repeatPassword) {
				$error = new ValidationError();
				$error->message = "The entered passwords do not match";
				$error->location = "repeat-password";
				$this->errors[] = $error;
			}
		}

		/**
		 * Checks if the given username is occupied by someone else. If there are errors, write them in the class field $errors.
		 * @param  mixed $username Username
		 * @param  mixed $connect Connection to DB
		 * @return void
		 */
		private function checkAvailableUsernameInDB($username, $connect) {
			$sql = "SELECT * FROM `users` WHERE `username` = '$username'";
			$findUser = mysqli_query($connect, $sql);
			
			if(mysqli_num_rows($findUser) != 0) {
				$error = new ValidationError();
				$error->message = "A user with the same Username already exists";
				$error->location = "username";
				$this->errors[] = $error;
			}
		}
	
		/**
		 * Checks for errors in the $errors class field. If present, creates a response to the client in JSON format.
		 * Response format:
		 * {
		 * 	'status': 'OK',
		 * 	'type': 'VALIDATION-ERROR',
		 * 	'data': $errors
		 * }
		 * @return void
		 */
		private function errorChecking() {
			if (!empty($this->errors)) {
				sendValidationErrorAnswer($this->errors);
			}
		}

		/**
		 * Validates the problem name. If there are errors, write them in the class field $errors.
		 * @param  mixed $problemName problem name
		 * @return void
		 */
		private function validateProblemName($problemName) {
			$error = new ValidationError();
			$error->location = "name";
			if (empty($problemName)) {
				$error->message = "Enter Problem name";
			} else {
				if (preg_match($this->patterns['pattern_problemName'], $problemName)) {
					$error->message = "Name must not contain punctuation marks";
				}

				if (strlen($problemName) > 25 || strlen($problemName) < 3) {
					$error->message = "Name must be between 3 and 25 characters";
				}
			}

			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the problem priority. If there are errors, write them in the class field $errors.
		 * @param  mixed $priority problem priority
		 * @return void
		 */
		private function validateProblemPriority($priority) {
			$error = new ValidationError();
			$error->location = "priority";
			if (empty($priority)) {
				$error->message = "Enter Priority";
			} else {
				if ($priority != 'Low' && $priority != 'Normal' && $priority != 'High') { //TODO: enum?
					$error->message = "Choose one of the options: Low, Normal, High";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the problem date. If there are errors, write them in the class field $errors.
		 * @param  mixed $date problem date
		 * @return void
		 */
		private function validateProblemDate($date) {
			$error = new ValidationError();
			$error->location = "date";
			if (empty($date)) {
				$error->message = "Enter Date";
			} else {
				if (!$this->validateDate($date)) {
					$error->message = "Invalid format. Select a date from the list or enter in the format YYYY-MM-DD";
				}
			}
			if ($error->message != '') {
				$this->errors[] = $error;
			}

		}

		/**
		 * Validates the problem description. If there are errors, write them in the class field $errors.
		 * @param  mixed $description problem description
		 * @return void
		 */
		private function validateProblemDescription($description) {
			$error = new ValidationError();
			$error->location = "description";
			if (strlen($description) > 600) {
				$error->message = "Description must not exceed 600 characters";
			}
			
			if ($error->message != '') {
				$this->errors[] = $error;
			}
		}

		/**
		 * Validates the date.
		 * @param  mixed $date date
		 * @return void
		 */
		private function validateDate($date, $format = 'Y-m-d'){
			$d = DateTime::createFromFormat($format, $date);
			return $d && $d->format($format) === $date;
		}

		/**
		 * Validates the registration form. If there are errors sends them to the client and exits the script.
		 * @return void 
		 */
		public function validateRegistrationForm() {
			$firstName = $lastName = $username = $eMail = $password = $repeatPassword = "";
			global $connect; //TODO: remove global
			
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$firstName = test_input($_POST["firstName"]);
				$lastName = test_input($_POST["lastName"]);
				$username = test_input($_POST["username"]);
				$eMail = test_input($_POST["eMail"]);
				$password = test_input($_POST["password"]);
				$repeatPassword = test_input($_POST["repeatPassword"]);
			}
			$this->validateFirstName($firstName);
			$this->validateLastName($lastName);
			$this->validateUsername($username);
			$this->checkAvailableUsernameInDB($username, $connect);
			$this->validateEMail($eMail);
			$this->validatePasswords($password, $repeatPassword);
			$this->errorChecking();
		}

		/**
		 * Validates the Create problem form. If there are errors sends them to the client and exits the script.
		 * @return void 
		 */
		public function validateCreateProblemForm() {
			$name = $priority = $description = $date = "";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$name = test_input($_POST['name']);
				$description = test_input($_POST['description']);
				$priority = test_input($_POST['priority']);
				$date = test_input($_POST['date']);
			}

			$this->validateProblemName($name);
			$this->validateProblemPriority($priority);
			$this->validateProblemDate($date);
			$this->validateProblemDescription($description);

			checkCSRFToken();
			$this->errorChecking();
		}

		/**
		 * Validates the login form. If there are errors sends them to the client and exits the script.
		 * @return void 
		 */
		public function validateLoginForm() {
			$username = $password = '';
			
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$username = test_input($_POST["username"]);
				$password = test_input($_POST["password"]);
			}

			$error = new ValidationError();
			$error->location = "username";
			if (empty($username)) {
				$error->message = "Enter Username";
			} 
			if ($error->message != '') {
				$this->errors[] = $error;
			}
			if (empty($password)) {
				$error = new ValidationError();
				$error->message = "Enter Password";
				$error->location = "password";
				$this->errors[] = $error;
			}

			$this->errorChecking();
		}
	}
?>