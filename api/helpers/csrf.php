<?php   
    /**
     * Checks the CSRF token that came with what is on the server side. If it doesn't match, it throws an error 403.
     * @return void
     */
    function checkCSRFToken(){
        if(!isset($_POST['csrf-token']) || empty($_POST['csrf-token']) || $_POST['csrf-token'] != $_SESSION['csrf-token']) {
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . 403 . ' ' . 'Your token has expired, please refresh the page. Or you are not logged into the application. If that\'s the case then stop sticking your filthy hands where they don\'t belong.');
            exit();
        }
    }
?>