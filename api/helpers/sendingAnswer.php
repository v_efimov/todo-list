<?php
    /**
     * Sends an empty response with 'OK' status and 'response' type.
     * @return void
     */
    function sendEmptyOKAnswer() {
        $answer = [
            'status' => 'OK',
            'type' => 'response'
        ];
        echo json_encode($answer);
        exit();
    }
    
    /**
     * Sends a validation error response with 'OK' status and 'VALIDATION-ERROR' type.
     * @param  mixed $errors errors
     * @return void
     */
    function sendValidationErrorAnswer($errors) {
        $answer = [
            'status' => 'OK',
            'type' => 'VALIDATION-ERROR',
            'data' => $errors
        ];
        echo json_encode($answer);
        exit();
    }
    
    /**
     * Sends a response with data with 'OK' status and 'response' type.
     * @param  mixed $data data
     * @return void
     */
    function sendAnswerWithData($data) {
        $answer = [
            'status' => 'OK',
            'type' => 'response',
            'data' => $data
        ];
        echo json_encode($answer, JSON_UNESCAPED_UNICODE);
        exit();
    }

    /**
     * Sends a list of issues along with an end-of-file flag to the user.
     * @param  mixed $problems problems
     * @param  mixed $isEnd end-of-file flag
     * @return void
     */
    function sendProblems($problems, $isEnd) {
        $responseToRequest = [
            'isEnd' => $isEnd,
            'problems' => $problems
        ];
        $answer = [
            'status' => 'OK',
            'type' => 'response',
            'data' => $responseToRequest
        ];
        $json = json_encode($answer, JSON_UNESCAPED_UNICODE);
        echo $json;
    }
?>