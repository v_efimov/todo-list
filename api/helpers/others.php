<?php
    /**
	 * Removes extra spaces and special characters from a string.
	 * @param mixed $data input string
	 * @return string modified string
	 */
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>