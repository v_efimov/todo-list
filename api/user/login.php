<?php
    include_once '../lib/Validation.php';
    include_once '../db/connect.php';
    include_once '../lib/ValidationError.php';
    include_once '../lib/User.php';
    include_once '../helpers/sendingAnswer.php';

    if(!isset($_SESSION)) { 
        session_start(); 
    } 

    $val = new Validation();
    $val->validateLoginForm();

    if (isUserLogged($connect, $_POST['username'], $_POST['password'])) {
        sendEmptyOKAnswer();
    } else {
        $error = new ValidationError();
        $error->location = 'password';
        $error->message = 'Wrong login or password';
        $errors[] = $error;
        sendValidationErrorAnswer($errors);
    }
?>