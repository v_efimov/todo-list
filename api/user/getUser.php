<?php
    include_once '../db/connect.php';
    include_once '../lib/User.php';
    include_once '../helpers/sendingAnswer.php';

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    
    $userId = $_SESSION['user'];
    $user = new User($userId);
    $dataToResponse = $user->getUserFromDBWithToken($connect);
    
    sendAnswerWithData($dataToResponse);
?>