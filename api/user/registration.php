<?php
    include_once '../lib/Validation.php';
    include_once '../lib/User.php';    
    include_once '../db/connect.php';
    include_once '../helpers/others.php';
    include_once '../helpers/sendingAnswer.php';

    $val = new Validation();
    $val->validateRegistrationForm();

    $firstName = test_input($_POST['firstName']);
    $lastName = test_input($_POST['lastName']);
    $username = test_input($_POST['username']);
    $eMail = test_input($_POST['eMail']);
    $password = test_input($_POST['password']);

    $newUser = new User($firstName, $lastName, $username, $eMail, $password);
    $newUser->addUserToDB($connect);
    
    sendEmptyOKAnswer();
?>