<?php
    include_once '../db/connect.php';
    include_once '../lib/Problem.php';
    include_once '../helpers/csrf.php';

    if(!isset($_SESSION)) { 
        session_start(); 
    } 

    checkCSRFToken();
    $userId = $_POST['user-id'];
    $problemId = $_POST['id'];
    $newProblemStatus = $_POST['status'];

    $changeableProblem = new Problem($problemId);
    $changeableProblem->changeProblemStatusInDB($newProblemStatus, $userId, $connect); 
?>