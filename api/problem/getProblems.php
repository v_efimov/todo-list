<?php
    //TODO: Refactor. Make a separate class for Problems?
    include_once '../lib/GetRequestParams.php';
    include_once '../db/connect.php';
    include_once '../helpers/sendingAnswer.php';

    $params = new GetRequestParams($_GET['user-id'], $_GET['status'], $_GET['sort'], $_GET['count'], $_GET['page']);

    $previousPage = ($params->page - 1) * $params->countProblems;
    $currentPage = $params->countProblems;
    $nextRowAfterCurrentPage = $currentPage + 1;

    $sql = 
        "SELECT * FROM `problems` 
        WHERE
            (CASE WHEN '$params->status' <> 'all' THEN `id_user` = '$params->userId' AND `status` = '$params->status' END) OR
            (CASE WHEN '$params->status' = 'all' THEN `id_user` = '$params->userId' END)
        ORDER BY 
            (CASE WHEN '$params->sortType' = 'up-to-down' THEN `date` END) DESC,
            (CASE WHEN '$params->sortType' = 'down-to-up' THEN `date` END) ASC
        LIMIT $previousPage, $nextRowAfterCurrentPage"
    ;
    
    $findProblems = mysqli_query($connect, $sql);
    $problems = [];
    $index = 1;
    $isEndOfData = true;

    foreach ($findProblems as $row) {
        if($index > $currentPage) {
            $isEndOfData = false;
            break;
        } else {
            $isEndOfData = true;
        }
        $problems[] = $row;
        $index++;
    }
    
    sendProblems($problems, $isEndOfData);
?>