<?php
    include_once '../lib/Problem.php';
    include_once '../lib/Validation.php';
    include_once '../db/connect.php';
    include_once '../helpers/sendingAnswer.php';

    $val = new Validation();
    $val->validateCreateProblemForm();

    $problemName = test_input($_POST['name']);
    $problemDescription = $_POST['description'];
    $problemPriority = test_input($_POST['priority']);
    $problemDate = test_input($_POST['date']);
    $problemStatus = test_input($_POST['status']);
    $userId = $_POST['user-id'];
    
    $newProblem = new Problem($problemName, $problemDescription, $problemPriority, $problemDate, $problemStatus);
    $newProblem->addProblemToDB($connect, $userId);
    
    sendEmptyOKAnswer();
?>