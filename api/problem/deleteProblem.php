<?php
    include_once '../db/connect.php';
    include_once '../helpers/csrf.php';
    include_once '../lib/Problem.php';

    if(!isset($_SESSION)) { 
        session_start(); 
    } 

    checkCSRFToken();
    $userId =  $_POST['user-id'];
    $problemId = $_POST['id'];
    
    $changeableProblem = new Problem($problemId);
    $changeableProblem->deleteProblemFromDB($userId, $connect); 
?>