<?php
    //TODO: Refactor. Make a separate class for Problems?
    include_once '../lib/GetRequestParams.php';
    include_once '../lib/GetRequestParams.php';
    include_once '../db/connect.php';
    include_once '../helpers/sendingAnswer.php';

    $params = new GetRequestParams($_GET['user-id'], $_GET['status'], $_GET['sort'], $_GET['count-displayed'], 0);
    
    $indexProblemToDisplay = $params->countProblems - 1;

    $sql = 
        "SELECT * FROM `problems` 
        WHERE
            (CASE WHEN '$params->status' <> 'all' THEN `id_user` = '$params->userId' AND `status` = '$params->status' END) OR
            (CASE WHEN '$params->status' = 'all' THEN `id_user` = '$params->userId' END)
        ORDER BY 
            (CASE WHEN '$params->sortType' = 'up-to-down' THEN `date` END) DESC,
            (CASE WHEN '$params->sortType' = 'down-to-up' THEN `date` END) ASC
        LIMIT $indexProblemToDisplay, 2"
    ;
    
    $findProblems = mysqli_query($connect, $sql);
    
    $problems = [];
    $index = 1;
    $isEndOfData = true;

    foreach ($findProblems as $row) {
        if($index > 1) {
            $isEndOfData = false;
            break;
        } else {
            $isEndOfData = true;
        }
        $problems[] = $row;
        $index++;
    }
    
    sendProblems($problems, $isEndOfData);
?>