<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/style.css">
    <title>Efimov's ToDo List</title>
    <link rel="icon" type="image/x-icon" href="./assets/images/favicon.ico">
    <script src="js/index.js" type="module"></script>
</head>
<body>
    <div class="main-page">
        <aside class="main-page__side-bar side-bar">
            <a href="./" class="side-bar__logo">
                <img class="side-bar__img" src="./assets/images/logo.png" alt="logo">
                <span class="side-bar__name">ToDo LIST</span>
            </a>
            <button class="side-bar__burger button button_tablet">
                <svg class="side-bar__burger-icon side-bar__burger-icon_burg">
                    <use xlink:href="./assets/icons/icons.svg#burger"></use>
                </svg>
                <svg class="side-bar__burger-icon side-bar__burger-icon_cross">
                    <use xlink:href="./assets/icons/icons.svg#cross"></use>
                </svg>
            </button>
            <ul class="side-bar__menu">
                <li class="menu-point" id="all">All problems</li>
                <li class="menu-point" id="active">Active</li>
                <li class="menu-point" id="completed">Completed</li>
                <li class="menu-point" id="deleted">Deleted</li>
            </ul>
        </aside>
        <main class="main-page__container">
            <header class="main-page__header header">
                <div class="header__create-problem-button"></div>
                <div class="header__profile"></div>
            </header>
            <div class="main-page__content">
                <div class="content">
                </div> 
                <template id="item">
                    <div class="items__item item">
                        <button class="item__action  item__action_restore" id="active">
                            <svg class="item__action-icon">
                                <use xlink:href="./assets/icons/icons.svg#repeat"></use>
                            </svg>
                        </button>
                        <button class="item__action  item__action_complete" id="completed">
                            <svg class="item__action-icon">
                                <use xlink:href="./assets/icons/icons.svg#complete"></use>
                            </svg>
                        </button>
                        <button class="item__action  item__action_delete" id="deleted">
                            <svg class="item__action-icon">
                                <use xlink:href="./assets/icons/icons.svg#delete"></use>
                            </svg>
                        </button>
                        <div class="item__type info-plate"></div>
                        <div class="item__title"></div>
                        <div class="item__description"></div>
                        <div class="item__footer">
                            <div class="item__date"></div>
                            <button class="item__button button button_primary">
                                More
                            </button>
                        </div>
                    </div>
                </template>
            </div>
        </main>
    </div>
    <div class="popup">
        <div class="popup__body">
            <button id="popup-close-btn" class="popup__close-btn">                
                <svg class="popup__close-btn-icon">
                    <use xlink:href="./assets/icons/icons.svg#cross"></use>
                </svg>
            </button>    
            <div class="popup__content"></div>
        </div>
    </div>
    <div class="alert-modal">
        <header class="alert-modal__header">
            <h3 class="alert-modal__title">Title</h3>
        </header>
        <div class="alert-modal__content"></div>
        <div class="alert-modal__close"> Click <u>here</u> to close </div>
    </div> 
</body>
</html>